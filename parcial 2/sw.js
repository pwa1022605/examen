self.addEventListener("fetch", event => {
    console.log(event.request.url);

    if (event.request.destination === "image") {
        event.respondWith(
            fetch('img/1.png')
        );
    }
    else if (event.request.url.includes("jsonplaceholder.typicode.com/todos")) {
        event.respondWith(fetch(event.request).then(response => {
            if (response.ok) {
                return response.json().then(data => {
                    const modifiedData = data.map(todo => {
                        return { ...todo, id: "ID: " + todo.id + " *"};
                    });
                    const modifiedResponse = new Response(JSON.stringify(modifiedData), {
                        status: response.status,
                        statusText: response.statusText,
                        headers: response.headers
                    });
                    return modifiedResponse;
                });
            }
            return response;
        }));
    }
});

self.addEventListener("install", event => {
    console.log("SW: Instalando Service Worker");
    const installing = new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log("SW: Instalacion Finalizada!");
        }, 1000);
        self.skipWaiting();
        resolve();
    });

    event.waitUntil(installing);
});

self.addEventListener("activate", event => {
    console.log("SW2: Service Worker Activo!");
});



