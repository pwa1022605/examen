if(navigator.serviceWorker){
    console.log("si es compatible");
    navigator.serviceWorker.register('/sw.js');
}

const url = "http://jsonplaceholder.typicode.com/todos";
const dataContainer = document.getElementById("data-container");
const dataT = document.getElementById("title");


function fetchAndDisplayData(url, callback) {
    fetch(url)
        .then(response => response.json())
        .then(data => callback(data))
        .catch(err => console.error(err));
}

function displayData(data) {
    dataContainer.innerHTML = ""; 
    dataT.innerHTML = "";

    data.forEach(item => {
        const listItem = document.createElement("div");
        listItem.textContent = JSON.stringify(item);
        dataContainer.appendChild(listItem);
    });
}

function getTodosPendientes() {
    fetchAndDisplayData(url, data => {
        dataT.innerHTML ="TODOS LOS IDs PENDIENTES:";
        const todosIDs = data.map(todo => "id: " + todo.id);
        dataContainer.textContent = todosIDs.join("\n");
    });
}

function getPendientesConTitulo() {
    fetchAndDisplayData(url, data => {
        dataT.innerHTML ="TODOS LOS IDs y TITLES PENDIENTES:";
        const todosIDSTitles = data.map(todo => "id: " + todo.id + "  title: " + todo.title);
        dataContainer.textContent = todosIDSTitles.join("\n");
    });
}

function getPendientesSinResolver() {
    fetchAndDisplayData(url, data => {
        dataT.innerHTML ="LISTADO DE PENDIENTES SIN RESOLVER (ID y TITLE):";

        const pendientesSinResolver = data.filter(todo => !todo.completed).map(todo => "id: " + todo.id + "  title: " + todo.title);
        dataContainer.textContent = pendientesSinResolver.join("\n");
    });
}

function getPendientesResueltos() {
    fetchAndDisplayData(url, data => {
        dataT.innerHTML ="LISTADO DE PENDIENTES RESUELTOS (ID y TITLE):";

        const pendientesResueltos = data.filter(todo => todo.completed).map(todo => "id: " + todo.id + "  title: " + todo.title);
        dataContainer.textContent = pendientesResueltos.join("\n");
    });
}

function getPendientesConUserID() {
    fetchAndDisplayData(url, data => {
        dataT.innerHTML ="LISTADO DE PENDIENTES (ID y userId):";

        const pendientesConUserID = data.map(todo => "id: " + todo.id + "  userId: " + todo.userId);
        dataContainer.textContent = pendientesConUserID.join("\n");
    });
}

function getPendientesResueltosConUserID() {
    fetchAndDisplayData(url, data => {
        dataT.innerHTML ="LISTADO DE PENDIENTES RESUELTOS (ID y userId):";

        const pendientesResueltosConUserID = data.filter(todo => todo.completed).map(todo => "id: " + todo.id + "  userId: " + todo.userId);
        dataContainer.textContent = pendientesResueltosConUserID.join("\n");
    });
}

function getPendientesSinResolverConUserID() {
    fetchAndDisplayData(url, data => {
        dataT.innerHTML ="LISTADO DE PENDIENTES SIN RESOLVER (ID y userId):";

        const pendientesSinResolverConUserID = data.filter(todo => !todo.completed).map(todo => "id: " + todo.id + "  userId: " + todo.userId);
        dataContainer.textContent = pendientesSinResolverConUserID.join("\n");
    });
}