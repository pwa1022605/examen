var url = "http://jsonplaceholder.typicode.com/todos";

const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

console.log("==== Menú ====");
console.log("1. Todos los IDs pendientes");
console.log("2. Todos los IDs y títulos pendientes");
console.log("3. Listado de pendientes sin resolver (ID y TITLE)");
console.log("4. Listado de pendientes resueltos (ID y TITLE)");
console.log("5. Listado de pendientes (ID y userId)");
console.log("6. Listado de pendientes resueltos (ID y userId)");
console.log("7. Listado de pendientes sin resolver (ID y userId)");
console.log("0. Salir");



rl.question("Ingrese el número de la opción deseada: ", function (opcion) {
    switch (parseInt(opcion)) {

        case 1:
            fetch(url)
                .then(response => response.json())
                .then(todos => {
                    console.log("TODOS LOS IDs PENDIENTES:");
                    todos.forEach(todo => {
                        console.log("id:", todo.id);
                    });

                })
                .catch(err => console.log(err));
            break;
        case 2:
            fetch(url)
                .then(response => response.json())
                .then(todos => {
                    console.log("TODOS LOS IDs y TITLES PENDIENTES:");
                    todos.forEach(todo => {
                        console.log("id:", todo.id + "  title: " + todo.title);
                    });
                })
                .catch(err => console.log(err));
            break;
        case 3:
            fetch(url)
                .then(response => response.json())
                .then(todos => {
                    console.log("LISTADO DE PENDIENTES SIN RESOLVER (ID y TITLE):");
                    todos.filter(todo => !todo.completed).forEach(todo => {
                        console.log("id:", todo.id + "  title: " + todo.title);
                    });
                })
                .catch(err => console.log(err));
            break;
        case 4:
            fetch(url)
                .then(response => response.json())
                .then(todos => {
                    console.log("LISTADO DE PENDIENTES RESUELTOS (ID y TITLE):");
                    todos.filter(todo => todo.completed).forEach(todo => {
                        console.log("id:", todo.id + "  title: " + todo.title);
                    });
                })
                .catch(err => console.log(err));
            break;
        case 5:
            fetch(url)
                .then(response => response.json())
                .then(todos => {
                    console.log("LISTADO DE PENDIENTES (ID y userId):");
                    todos.forEach(todo => {
                        console.log("id:", todo.id + " userId:" + todo.userId);
                    });
                })
                .catch(err => console.log(err));
            break;
        case 6:
            fetch(url)
                .then(response => response.json())
                .then(todos => {
                    console.log("LISTADO DE PENDIENTES RESUELTOS (ID y userId):");
                    todos.filter(todo => todo.completed).forEach(todo => {
                        console.log("id:", todo.id + " userId:" + todo.userId);
                    });
                })
                .catch(err => console.log(err));
            break;
        case 7:
            fetch(url)
                .then(response => response.json())
                .then(todos => {
                    console.log("LISTADO DE PENDIENTES SIN RESOLVER (ID y userId):");
                    todos.filter(todo => !todo.completed).forEach(todo => {
                        console.log("id:", todo.id + " userId:" + todo.userId);
                    });
                })
                .catch(err => console.log(err));
            break;
        case 0:
            console.log("Saliendo del programa");
            break;
        default:
            console.log("Opción no válida");
    }

    rl.close();
});